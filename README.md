# Upload torrent to tracker

## Dependencies
This script uses Python 3. You need to install these dependencies using `pip`:

- **Selene**
    - `pip install selene --pre`
- **BeautifulSoup**
    - `pip install bs4`
- **Selenium**
    - `pip install selenium`

In addition to the above, you also need Chrome or Chromium installed. This was tested using Chromium 68 and Chromedriver 2.38.

## Usage
Copy the `config.ini.example` into a new `config.ini` file and put it in your credentials.

**Read the help:**

`python upload.py -h`

## Amazon Bookmarklet 
In case you wouldn't like to use the script to extract the data automatically from Amazon, or Amazon has blocked automated requests for you, you can use the `bookmarklet` (`extract_from_amazon.min.js`) to pull data from your browser in JSON format. You can then use that JSON as an argument (`-json`) for the upload script.