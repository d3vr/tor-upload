// Bookmarklet used to extract book data from Amazon as JSON. Use -json flag in the upload script to use the data.
// Used in case Upload script is blocked or can't extract the data for whatever reason



// Select by XPATH
function x(xpathToExecute){
    var result = [];
    var nodesSnapshot = document.evaluate(xpathToExecute, document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null );
    for ( var i=0 ; i < nodesSnapshot.snapshotLength; i++ ){
      result.push( nodesSnapshot.snapshotItem(i) );
    }
    return result;
}
function s(q){ return document.querySelector(q); }
function ss(q){ return document.querySelectorAll(q); }

var data = {};
var got_img = false;

// Get title 
var title = s("#productTitle");
if(!title){
    title = s("#ebooksProductTitle");
    if(!title){
        alert("Title not found!");
    }
    data["title"] = title.textContent;
}else{
    data["title"] = title.textContent;
}

// Append edition to title
var edition = s("#bookEdition")
if(edition){
    title += " ("+edition.textContent+")"
}
var author_list = x('//div[@id="bylineInfo"]/span[contains(@class, "author")]//a[not(ancestor::table)][not(contains(@href, "javascript:"))]')
if(author_list.length){
    data["authors"] = [];
    author_list.forEach(function(item){
        data["authors"].push(item.textContent);
    });
}else{
    alert('Authors not found');
}

// img from thumbs
if(s("#imgThumbs")){
    data["img"] = "wip";
    s("#imgThumbs").querySelector("img").dispatchEvent(new Event("click", {bubbles: true}));
    var thumb_timer = setInterval(function(){
        if(!got_img && s('#igImage')){
            data["img"] = s('#igImage').src;
            got_img = true;
            document.querySelector('[data-action="a-popover-floating-close"]').remove();
            document.querySelector('[id="a-popover-lgtbox"]').remove();
            clearInterval(thumb_timer);
        }
    }, 100);
}
// Image from book preview 
if(typeof data["img"] == "undefined"){
    if(s('#sitbLogoImg') || s("#ebooksSitbLogoImg")){
        data["img"] = "wip";
        var element = s("#ebooksImgBlkFront") || s("#imgBlkFront");
        element.dispatchEvent(new Event("click", {bubbles:true}));
        var lookinside_time = setInterval(function(){
            if(!got_img){
                if(x('//div[@id="sitbReaderHighlights-1"]/following-sibling::img')[0]){
                    data["img"] = x('//div[@id="sitbReaderHighlights-1"]/following-sibling::img')[0].src;
                    got_img = true;
                }else if(s('[name="coverimage"]')){
                    data["img"] = s('[name="coverimage"]').src
                    got_img = true;
                }else if(s("#sitbReaderFrame")){
                    data["img"] = s("#sitbReaderFrame").contentDocument.querySelector("img").src
                    got_img = true;
                }else if(s("#sitbReaderPageContainer img")){
                    data["img"] = s("#sitbReaderPageContainer img").src;
                    got_img = true; 
                }else{
                    console.log("No image selector found");
                }
            }else{
                s('#sitbReaderCloseButton').click();
                clearInterval(lookinside_time);
            }
        }, 100)
    }
}
if(typeof data["img"] == "undefined"){
    var img = x('//div[@id="mainImageContainer"]//img')
    if(img.length){
        data["img"] = img[0].src;
        got_img = true;
    }else{
        img = x('//div[@id="img-canvas"]//img')
        if(img.length){
            data["img"] = img[0].src;
            got_img = true;
        }else{
            img = x('//img[@id="ebooksImgBlkFront"]')
            data["img"] = img[0].src;
            got_img = true;
        }
    }
}

var details = x('//table[@id="productDetailsTable"]//div[@class="content"]/ul/li')
var details_text = "";
var dc = prompt("Details count?", 5);
if(details.length){
    details_text = "<ul>";
    details.slice(0, dc).forEach(function(item){
        details_text += item.outerHTML;
    })
    details_text += "</ul>";
}

iframe = s("#bookDesc_iframe");
var description = "";
if(iframe){
    description = iframe.contentDocument.querySelector("#iframeContent").innerHTML;
}
if(details_text && description){
    description += "<br><br><hr><br><br>"+details_text;
}
data["description"] = description;
if(data){
    if(got_img){
        prompt("JSON data", JSON.stringify(data));
        window.close();
    }else{
        var img_timer = setInterval(function(){
            if(got_img){
                prompt("JSON data", JSON.stringify(data));
                window.close();
                clearInterval(img_timer);
            }
        }, 100)
    }
}