# Upload torrent file to tracker with extracted info from given Goodreads page
import argparse
import sys
import configparser
import json
from urllib.request import urlopen
from bs4 import BeautifulSoup

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import Select
from selene.api import *
from selenium.common.exceptions import NoSuchElementException
import textwrap

# Load configuration
settings = configparser.ConfigParser()
settings.read(sys.path[0]+'/config.ini')

try:
	take_screenshot = int(settings.get('debug', 'screenshot'))
	path_to_screenshot = settings.get('debug', 'screenshot_path')
	log = settings.get('debug', 'log')
	logfile = settings.get('debug', 'logfile')
	tracker_url = settings.get('tracker', 'url')
	email = settings.get('auth', 'username')
	password = settings.get('auth', 'password')
	timeout = settings.get('settings', 'timeout')
	reports_folder = settings.get('settings', 'reports_folder')
except:
	print("Config file or options missing. Make sure you have a config.ini set up and you have all options from config.ini.example present")
	sys.exit()

config.timeout = int(timeout)
config.reports_folder = reports_folder

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=textwrap.dedent("""Upload torrent file to tracker with extracted info from given Goodreads or Amazon page.
+--------------------------------+------------------------------+
|                            Categories                         |
+--------------------------------+------------------------------+
| bio   == Biographical          |  biz  == Business             |
| aa    == Action/Adventure      |  comp  == Computer/Internet   |
| ct    == Crime/Thriller        |  gf  == General Fiction       |
| gnf   == General Non-Fiction   |  hf == Historical Fiction     |
| his   == History               |  hor  == Horror               |
| hum   == Humor                 |  lc  == Literary Classics     |
| sci   == Math/Science/Tech     |  med  == Medical              |
| mys   == Mystery               |  mag  == Magazines/Newspapers |
| psr   == Pol/Soc/Relig         |  rom  == Romance              |
| sf    == Science Fiction       |  sh  == Self-Help             |
| ya    == Young Adult           |  tc == True Crime             |
| uf    == Urban Fantasy         |                               |
|                                |                               |
| ce    == Complete Editions     | art  == Art                   |
| cg    == Comics/Graphic novels | crf == Crafts                 |
| fan   == Fantasy               | food == Food                  |
| hg    == Home/Garden           | magic == Illusion/Magic       |
| inst  == Instructional         | juv  == Juvenile              |
| lang  == Language              | lgbt == LGBT                  |
| mc    == Mixed Collections     | nat == Nature                 |
| philo == Philosophy            | rec  == Recreation            |
| ta    == Travel/Adventure      | west  == Western              |
+------------------------------+---------------------------------+
	"""))

categories = dict(bio=72, aa=60, ct=62, gnf=74, his=76, hum=103, sci=80, mys=94, psr=81, sf=69, ya=112, uf=109, biz=90, comp=73, gf=64, hf=102, hor=65, lc=67, med=92, mag=79, rom=68, sh=75, tc=104, art=71, crf=101, food=107, magic=115, juv=66, lgbt=138, nat=120, rec=82, west=70, ce=17, cg=61, fan=63, hg=77, inst=91, lang=78, mc=118, philo=95, ta=96)

parser.add_argument('path_to_file', type=str, help='Path to the file which will be added to the torrent')
parser.add_argument('-isbn', dest='isbn', help='ISBN (optional)')
parser.add_argument('-title', dest='title', help='Title (optional)')
parser.add_argument('-desc', dest='description', help='Description (optional)')
parser.add_argument('-auth', dest='authors', help='Authors (JSON array) (optional)')
parser.add_argument('-narr', dest='narrators', help='Narrators (JSON array) (optional)')
parser.add_argument('-img', dest='img', help='Book cover (optional)')
parser.add_argument('-gr', dest='gr', metavar='link_to_goodreads_page', help='Link to the book\'s Goodreads page')
parser.add_argument('-az', dest='az', metavar='link_to_amazon_page', help='Link to the book\'s Amazon page')
parser.add_argument('-json', dest='json', help='Torrent data in JSON')
parser.add_argument('-cat', dest='cat', metavar='category', help='Category id', required=True)
parser.add_argument('-ne', dest='noedition', metavar='noedition', help="Don't include edition in title")
parser.add_argument('-dc', dest='details_count', metavar='details_count', help="Number of details to take from Amazon page")
parser.add_argument('-series', dest='series', metavar='series', help='Series')
parser.add_argument('-snumber', dest='snumber', metavar='snumber', help='Number in Series')
parser.add_argument('-lang', dest='lang', metavar='language', default="English", help='Language name (default: English)')
parser.add_argument('-crude', dest='crude', default=0, help='0|1 Mark torrent as containing crude language.')
parser.add_argument('-violence', dest='violence', default=0, help='0|1 Mark torrent as containing violence.')
parser.add_argument('-ssex', dest='ssex', default=0, help='0|1 Mark torrent as containing some sexual content.')
parser.add_argument('-esex', dest='esex', default=0, help='0|1 Mark torrent as containing explicit sexual content.')
parser.add_argument('-abridged', dest='abridged', default=0, help='0|1 Mark torrent as abridged.')
parser.add_argument('-lgbt', dest='lgbt', default=0, help='0|1 Mark torrent as containing LGBT topics.')
parser.add_argument('-vip', dest='vip', default=0, help='0|1 Mark torrent as vip. (Default: 0)')
parser.add_argument('-tags', dest='tags', help='Extra tags')

args = parser.parse_args()
gr = args.gr
az = args.az
json_data = json.loads(args.json) if args.json else ""
cat = categories[args.cat]
lang = args.lang
crude = args.crude
violence = args.violence
ssex = args.ssex
esex = args.esex
abridged = args.abridged
lgbt = args.lgbt
vip = args.vip
isbn = args.isbn if args.isbn else ""
title = args.title if args.title else ""
authors = json.loads(args.authors) if args.authors else []
narrators = json.loads(args.narrators) if args.narrators else []
img = args.img if args.img else ""
description = args.description if args.description else ""
extra_tags = args.tags if args.tags else ""
noedition = args.noedition if args.noedition else False
details_count = int(args.details_count) if args.details_count else 5
series = args.series if args.series else ""
serie_number = args.snumber if args.snumber else ""
torrent_path = args.path_to_file

# Initialize driver
chrome_options = Options()
chrome_options.add_argument("--disable-dev-shm-usage")
chrome_options.add_argument("--no-sandbox")
chrome_options.add_argument("--disable-notifications")
chrome_options.add_argument("--disable-infobars")
chrome_options.add_argument("--headless")
chrome_options.add_argument("--window-size=1366,768")

print("Opening browser")
driver = webdriver.Chrome(chrome_options=chrome_options)
browser.set_driver(driver)
print("Opened browser")

serie_field = series
tags = extra_tags


# Use a Goodreads link to pull the details
if gr:
	print("Opening Goodreads page")
	response = urlopen(gr)
	html = response.read()

	print("Response received")
	soup = BeautifulSoup(html, 'lxml')

	print("Extracting data")
	title = ''.join(soup.find(attrs={'id':'bookTitle'}).find_all(text=True, recursive=False)).strip()
	if not len(authors):
		authors_list = soup.select('.authorName')
		authors = []
		for author in authors_list:
			author_name = author.text.strip()
			authors.append(author_name) if "(" not in author.text else ""
	if not img:
		img = soup.find(attrs={'id':'coverImage'}).get("src")

	description = soup.select('#description span')[-1].decode_contents(formatter="html")+ '<br/><br/><a href="'+gr+'"><img style="vertical-align: middle;" src="http://d.gr-assets.com/misc/1454549143-1454549143_goodreads_misc.png" width="20" height="20" data-mce-src="http://d.gr-assets.com/misc/1454549143-1454549143_goodreads_misc.png" data-mce-style="vertical-align: middle;"> The book on Goodreads</a>'

	isbn = soup.find(attrs={'itemprop':'isbn'}).text if soup.find(attrs={'itemprop':'isbn'}) else ""
	serie = soup.select("#bookTitle .greyText")
	serie = serie[0].text.strip().strip("()") if serie else ""
	try:
		serie_field = serie[0:serie.index("#")]
	except:
		serie_field = serie
	serie = ", "+serie if serie else ""
	pages = soup.find(attrs={'itemprop':'numberOfPages'}).text if soup.find(attrs={'itemprop':'numberOfPages'}) else ""
	published = " ".join(soup.select("#details .row")[1].text.strip().split())

	try:
		tags = extra_tags+", "+pages+", "+published[0:published.index("by")]+serie
	except:
		tags = extra_tags+", "+pages+", "+published+serie

# Use an Amazon link to pull the details
elif az:
	print("Opening Amazon page")
	browser.open_url(az)

	print("Extracting data")

	if not title:
		# Normal edition
		try:
			title = driver.find_element_by_id('productTitle').text
		except:
			# Kindle edition
			try:
				title = driver.find_element_by_id('ebooksProductTitle').text
			except:
				print("Can't find title on Amazon page. Amazon is probably blocking automated requests. Check the log for details: "+logfile)
				if log:
					f = open(logfile, "a")
					f.write(driver.page_source)
					if take_screenshot:
						print("Taking screenshot")
						driver.get_screenshot_as_file(path_to_screenshot)
				driver.quit()
				sys.exit()
		if not noedition:
			try:
				edition = driver.find_element_by_id('bookEdition')
				if edition:
					title = title +" ("+edition.text+")"
			except:
				pass
	if not len(authors):
		authors_list = driver.find_elements_by_xpath('//div[@id="bylineInfo"]/span[contains(@class, "author")]//a[not(ancestor::table)][not(contains(@href, "javascript:"))]')
		authors = []
		for author in authors_list:
			authors.append(author.text)
	
	if not img:
		# Available images under the cover
		try:
			driver.execute_script('document.getElementById("imgThumbs").querySelector("img").dispatchEvent(new Event("click", {bubbles: true}))')
			img = driver.find_element_by_id('igImage').get_attribute('src')
			driver.execute_script('document.querySelector(\'button[data-action="a-popover-close"]\').dispatchEvent(new Event("click", {bubbles: true}))')
		except:
			# Big image from the book preview
			try:
				lookinside =  driver.find_element_by_css_selector('#sitbLogoImg') or driver.find_element_by_css_selector("#ebooksSitbLogoImg")
				if lookinside:
					driver.execute_script('var element = document.getElementById("ebooksImgBlkFront") || document.getElementById("imgBlkFront"); element.dispatchEvent(new Event("click", {bubbles:true}))')
					try:
						s('[name="coverimage"]').should(be.visible)
					except:
						s('#sitbReaderHighlights-1').should(be.visible)

					img = driver.find_element_by_xpath('//div[@id="sitbReaderHighlights-1"]/following-sibling::img').get_attribute('src')
					driver.find_element_by_id('sitbReaderCloseButton').click()
			# Different identifiers for the cover image
			except:
				try:
					img = driver.find_element_by_xpath('//div[@id="mainImageContainer"]//img').get_attribute('src')
				except:
					try:
						img = driver.find_element_by_xpath('//div[@id="img-canvas"]//img').get_attribute('src')
					except:
						try:
							img = driver.find_element_by_xpath('//img[@id="ebooksImgBlkFront"]').get_attribute('src')
						except:
							try:
								img = driver.find_element_by_xpath('//img[@id="main-image"]').get_attribute('src')
							except:
								img = ""

	details_text = ''
	try:
		try:
			details = driver.find_elements_by_xpath('//table[@id="productDetailsTable"]//div[@class="content"]/ul/li')
		except:
			details = driver.find_elements_by_xpath('//div[@id="detail_bullets_id"]//table//div[@class="content"]/ul/li')
		
		if details and len(details) > details_count: 
			details_text += "<ul>"
			for detail in details[:details_count]:
				details_text += detail.get_attribute('outerHTML')
			details_text += "</ul>"
	except:
		pass

	if not description:
		try:
			iframe = driver.find_element_by_id('bookDesc_iframe')
			driver.switch_to_frame(iframe)
			description = driver.find_element_by_id("iframeContent").get_attribute('innerHTML')
		except:
			print("Can't find description in Amazon page")
			pass
	if details_text:
		if description:
			description += "<br><br><hr><br><br>"
		description += details_text
# Use JSON data (extracted using the bookmarklet)
elif json_data:
	title = json_data['title']
	img = json_data['img']
	description = json_data['description']
	if not len(authors):
		authors = json_data['authors']

print("Opening tracker upload page")
browser.open_url(tracker_url+"/tor/upload.php")

print("Logging in")
driver.find_element_by_css_selector("[name=email]").send_keys(email)
driver.find_element_by_css_selector("[name=password]").send_keys(password)
driver.find_element_by_css_selector("[type=submit]").click()

s('[type="file"]').should(be.visible)

print("Uploading torrent file")
driver.find_element_by_css_selector("[type=file]").send_keys(torrent_path)
driver.find_element_by_css_selector("[value=Submit]").click()

s('[name="tor[isbn]"]').should(be.visible)

print("Filling in data")
driver.find_element_by_css_selector("[name='tor[isbn]']").send_keys(isbn)
driver.find_element_by_css_selector("[name='tor[title]']").send_keys(title)

# If there's more than one author, create the inputs for them
if len(authors)>1:
	plus = driver.find_element_by_css_selector('.plusDiv')
	for author in authors[:len(authors)-1]:
		driver.execute_script('arguments[0].style.display = "inline-block"', plus)
		plus.click()
	author_inputs = driver.find_elements_by_css_selector("[name='tor[author][]']")
	for index, author_input in enumerate(author_inputs):
		author_input.send_keys(authors[index])
# Otherwise insert the author name directly
else:
	driver.find_element_by_css_selector("[name='tor[author][]']").send_keys(authors[0])

if len(narrators):
	if len(narrators)>1:
		plus = driver.find_element_by_xpath("//input[@name='tor[narrator][]']/parent::td/a[@class='plusDiv']")
		for narrator in narrators[:len(narrators)-1]:
			driver.execute_script('arguments[0].style.display = "inline-block"', plus)
			plus.click()
		narrator_inputs = driver.find_elements_by_css_selector("[name='tor[narrator][]']")
		for index, narrator_input in enumerate(narrator_inputs):
			narrator_input.send_keys(narrators[index])	
	else:
		driver.find_element_by_css_selector("[name='tor[narrator][]']").send_keys(authors[0])

driver.find_element_by_css_selector("[name='tor[series][0][name]']").send_keys(serie_field) if serie_field else ""
driver.find_element_by_css_selector("[name='tor[series][0][extra]']").send_keys(serie_number) if serie_number else ""
driver.find_element_by_css_selector("[name='tor[tags]']").send_keys(tags)
driver.find_element_by_css_selector("[name='tor[posterURL]']").send_keys(img)
Select(driver.find_element_by_css_selector("[name='tor[category]']")).select_by_value(str(cat))
Select(driver.find_element_by_css_selector("[name='tor[language]']")).select_by_visible_text(lang)
# Crude language
if int(crude):
	driver.find_element_by_css_selector("[name='crudeLang']").click()
if int(violence):
	driver.find_element_by_css_selector("[name='violence']").click()
# Some sexual content
if int(ssex):
	driver.find_element_by_css_selector("[name='sSex']").click()
# Explicit sexual content
if int(esex):
	driver.find_element_by_css_selector("[name='eSex']").click()
if int(abridged):
	driver.find_element_by_css_selector("[name='abridged']").click()
if int(lgbt):
	driver.find_element_by_css_selector("[name='lgbt']").click()
# Mark torrent as VIP only
if int(vip):
	driver.find_element_by_css_selector("[name='tor[vip]']").click()
	

driver.execute_script("document.querySelector('iframe').contentWindow.document.querySelector('#tinymce').innerHTML = '"+description.replace("'","\\'").replace('\n', ' ').replace('\r', '')+"'")
print("Submitting torrent")
driver.find_element_by_css_selector("input[value='Submit']").click()

page_url = driver.current_url
try:
	print("View: "+page_url)
	print("Edit: "+tracker_url+"/edit.php?id="+page_url[page_url.index("/t/")+3:page_url.index("?")])
except ValueError:
	print('Upload not successful')

# Take screenshot for debugging
if take_screenshot:
	print("Taking screenshot")
	driver.get_screenshot_as_file(path_to_screenshot)

driver.quit()